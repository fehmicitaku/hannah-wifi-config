var ApiHandler = function(req, res) {
	this.req = req;
	this.res = res;
};

ApiHandler.prototype.require = function(required) {
	var _this = this;
	return new Promise(function(resolve, reject) {
		var checkExists = function(data) {
			return data !== null && typeof data != 'undefined' && data != "undefined";
		};
		if (required.body) {
			console.log("body is", body);
			for (var i = 0; i < required.body.length; i++) {
				if (required.multiple) {
					var error = false;
					for (var j = 0; j < _this.req.body.length; j++) {
						if (!checkExists(_this.req.body[j][required.body[i]])) {
							error = true;
							j = _this.req.body[required.body[i]].length;
						}
					}
					if (error) {
						_this.error('Missing parameters.');
						break;
					}
				} else if (!checkExists(_this.req.body[required.body[i]])) {
					_this.error('Missing parameter "' + required.body[i] + '"');
					break;
				}
			}
		}
		if (required.params && _this._error) {
			for (var i = 0; i < required.params.length; i++) {
				if (!checkExists(_this.req.params[required.params[i]])) {
					_this.error('Missing parameter "' + required.params[i] + '"');
					break;
				}
			}
		}
		if (!_this._error) {
			resolve();
		}
	});
};


ApiHandler.prototype.status = function (status) {
	this.statusCode = status;
	return this;
};

ApiHandler.prototype.error = function (message) {
	if (this.res.headerSent) {
		return;
	}
	this._error = true;
	this.res.status(this.statusCode || 400).json({
		isError: true,
		message: message
	});
};

ApiHandler.prototype.success = function (data) {
	if (this.res.headerSent) {
		return;
	}
	this.res.status(this.statusCode || 200).json({
		isError: false,
		object: data
	});
};

ApiHandler.prototype.unauthorized = function() {
	this.statusCode = 403;
	this.error('Unauthorized');
};

ApiHandler.prototype.redirect = function (url) {
	if (this.res.headerSent) {
		return;
	}
	this.res.writeHead(302, {
	  'Location': url
	});
	this.res.end();
};

module.exports = ApiHandler;