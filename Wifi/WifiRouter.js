'use strict';

let WifiController = require('./WifiController');

module.exports = function(app) {
    app.get('/api/getAvaliableWifis', WifiController.getAvaliableWifis);
    app.get('/api/openHotspot',WifiController.openHotspot);
    app.get('/api/closeHotspot', WifiController.turnOffHotspot);
    app.post('/api/connectWiFi', WifiController.connectToWifi);
    app.post('/api/saveWifiList',WifiController.saveWifiList);
    app.get('/api/readtxt',WifiController.readTextfile);
    app.post('/api/closeHotspotAndConnectToWifi', WifiController.closeHotspotAndConnectToWifi);
}