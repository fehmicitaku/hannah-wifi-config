var Promise = require('bluebird');
var exec = require('child_process').exec;
let fs = require('fs');
var ApiHandler = require('../lib/ApiHandler');

let WifiController = {};

WifiController.getAvaliableWifis = function(req,res) {
    let handler = new ApiHandler(req,res);
    exec("sudo nmcli -f SSID dev wifi list",function(err,stdout) {
        if(err) {
            console.log("err",err);
            handler.error("Please try again");
        } else {
            handler.success(stdout);
	    console.log(stdout);
        }       
    })    
}

WifiController.openHotspot = function(req,res) {
    let handler = new ApiHandler(req,res);
    exec("sudo nmcli connection up Hotspot", function(err,stdout) {
        if(err) {
            console.log("err",err);
            handler.error("Error while trying to set up the hotspot...")
        } else {
            handler.success(stdout);
        }
    })
}

WifiController.turnOffHotspot = function(req,res) {
    let handler = new ApiHandler(req,res);
    exec("sudo nmcli connection down Hotspot", function(err,stdout) {
        if(err) {
            console.log("err",err);
            handler.error("Error while trying to turn off the Hotspot...")
        } else {
            handler.success(stdout);
        }
    })
};

WifiController.connectToWifi = function(req,res) {
    let handler = new ApiHandler(req,res);
    let ssid = req.body.ssid;
    let password = req.body.password;
    exec(`sudo nmcli device wifi con ${ssid} password ${password}`,function(err,stdout) {
        if(err) {
            console.log("err",err);
            handler.error("Error while connecting on provided Wi-Fi...")
        } else {
            handler.success(stdout);
        }
    })
}

WifiController.saveWifiList = function(req,res) {
    let handler = new ApiHandler(req,res);
    let list = req.body;
    console.log(req.body);
    var stringifiedList = "";
    for(var x in list) {
        stringifiedList += list[x] + "\n";
    }

    fs.writeFile('wifilist.txt',stringifiedList,(error) => {
        if(error) {
            handler.error(error);
        } else {
            handler.success("Successfully wrote wifi list");
        }
    })
}

WifiController.readTextfile = function(req,res) {
    let handler = new ApiHandler(req,res);

    fs.readFile('./wifilist.txt', (error,txt) => {
        if(error) {
            handler.error(error)
        } else {
            handler.success(txt.toString());
        }
    })
}

WifiController.closeHotspotAndConnectToWifi = function(req,res) {
    let handler = new ApiHandler(req,res);
    exec("sudo nmcli connection down Hotspot", function(err,stdout) {
        if(err) {
            console.log("err",err);
            handler.error("Error while trying to turn off the Hotspot...")
        } else {
            let ssid = req.body.ssid;
            let password = req.body.password;
            setTimeout(function() {
                exec(`sudo nmcli device wifi con ${ssid} password ${password}`,function(err,stdout) {
                    if(err) {
                        console.log("err",err);
                        handler.error("Error while connecting on provided Wi-Fi...")
                    } else {
                        handler.success(stdout);
                    }
                })
            },5000);
            // handler.success(stdout);
        }
    })
}


module.exports = WifiController;
