var express = require('express');
var bodyParser = require('body-parser');
var exec = require('child_process').exec;
var ApiHandler = require('./lib/ApiHandler');
var Promise = require('bluebird');

var port = 3100;
var app = express();


app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, token");
	next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({
	limit: '20mb'
}));

require('./Wifi/WifiRouter')(app);

app.listen(port, function() {
    console.log("Successfully started Wifi Config... at port", port);
})